
var app = angular.module('AcmeBankApp', [
  'ngRoute'
  ]);

/**
 * Configure the Routes
 */
 app.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
    // Home
    .when("/", {templateUrl: "partials/home.html", controller: "PageCtrl"})
    // Pages
    .when("/transact_history", {templateUrl: "partials/transact_history.html", controller: "PageCtrl"})
    // else 404
    .otherwise("/404", {templateUrl: "partials/404.html", controller: "PageCtrl"});
  }]);


 app.controller('PageCtrl', function ($scope, $rootScope, $http, $location, $route, $window, $timeout) {

  $http({
    method : "GET",
    url : "js/bank.json"
  }).then(function mySucces(response) {

    $rootScope.mydata = response.data.records;
    $rootScope.mydata2 = response.data.records[0].typeofaccounts;
    var obj = response.data;
  }, function myError(response) {
    $rootScope.mydata = response.statusText;
  });


  $scope.deposit = function($index) {
    localStorage.setItem('balance', $index);
    $scope.task =  "DEPOSIT CASH";
  };

  $scope.withdraw = function($index) {
    localStorage.setItem('withdraw', $index);
    $scope.task =  "WITHDRAW CASH";
  };

  var now = new Date().toLocaleString();


  $scope.savedeposit = function() {

   if ($scope.money != null) {

     var added = $scope.money;
     var addprefix = "+"+added;
     var balance = localStorage.getItem('balance');
     var balance2 =  $rootScope.mydata2[balance];
     var fullamount =  +added + +balance2.balance; 
     



     $rootScope.mydata2[balance].balance = fullamount;

     $rootScope.mydata2[balance].history.push({
      "id": $rootScope.mydata2.length + 1,
      "balance": fullamount,
      "amount": addprefix,
      "date": now
    });

    $scope.amount = "Thank you for banking with Acme, your balance is now: " + fullamount;
    localStorage.removeItem('balance');  
    $scope.money = '';




  }

}

$scope.savewithdraw = function() {
 if ($scope.money != null) {

   var minus = $scope.money;
   var addprefix = "-"+minus;

   var balance = localStorage.getItem('withdraw');
   var balance2 =  $rootScope.mydata2[balance];
   var fullamount =  +balance2.balance - +minus; 


   if (fullamount <= 1000 && $rootScope.mydata2[balance].accountname == "savings") {

    alert("cant withdraw your account below R1000,00");
    return;
    
  }
  if (fullamount <= -100000 && $rootScope.mydata2[balance].accountname == "current") {

    alert("Cant withdraw beoynd your overdraft of R100000,00");
    return;
    
  }

  $rootScope.mydata2[balance].history.push({
    "id": $rootScope.mydata2.length + 1,
    "balance": fullamount,
    "amount": addprefix,
    "date": now
  });

  $rootScope.mydata2[balance].balance = fullamount;
  $scope.amount = "Thank you for banking with Acme, your REMAINING balance is now: " + fullamount;
  localStorage.removeItem('withdraw');  
  $scope.money = '';

  return;


}

}


$scope.view = function($index) {

$scope.history = $rootScope.mydata2[$index].history;
  $scope.task =  "TRANSCATION HISTORY";


};



});